const urlJoin = require("url-join");
const { ControlledContainerMixin, getContainerFromUri } = require("@semapps/ldp");
const { MIME_TYPES } = require("@semapps/mime-types");
const { ACTOR_TYPES, ACTIVITY_TYPES, PUBLIC_URI } = require("@semapps/activitypub");

module.exports = {
  name: 'minicourses.trainings',
  mixins: [ControlledContainerMixin],
  settings: {
    baseUrl: null,
    announcerUri: null,
    // Container settings
    path: '/trainings',
    acceptedTypes: ['heco:Training', ACTOR_TYPES.APPLICATION],
    dereference: ['sec:publicKey'],
  },
  actions: {
    async announce(ctx) {
      const { trainingUri } = ctx.params;

      if (this.settings.announcerUri) {
        const announcer = ctx.call('activitypub.actor.get', { actorUri: this.settings.announcerUri, webId: 'system' });

        this.logger.info('Announcing training ' + trainingUri + ' through ' + announcer.id);

        await ctx.call(
          'activitypub.outbox.post',
          {
            collectionUri: announcer.outbox,
            '@context': 'https://www.w3.org/ns/activitystreams',
            actor: announcer.id,
            type: ACTIVITY_TYPES.CREATE,
            object: trainingUri,
            to: [announcer.followers, PUBLIC_URI]
          },
          { meta: { webId: announcer.id } }
        );
      }
    },
    async updateDuration(ctx) {
      const { trainingUri } = ctx.params;

      const results = await ctx.call('triplestore.query', {
        query: `
          PREFIX tutor: <http://virtual-assembly.org/ontologies/pair-tutor#>
          PREFIX pair: <http://virtual-assembly.org/ontologies/pair#>
          SELECT (SUM(?duration) as ?sum)
          WHERE {
            <${trainingUri}> pair:hasPart ?lessonUri .
            ?lessonUri tutor:duration ?duration .
          }
        `,
        accept: MIME_TYPES.JSON,
        webId: 'system'
      });

      const totalDuration = results[0].sum.value;

      const training = await this.actions.get({
        resourceUri: trainingUri,
        accept: MIME_TYPES.JSON,
        webId: 'system'
      }, { parentCtx: ctx });

      await this.actions.put({
        resource: {
          ...training,
          'tutor:duration': totalDuration,
        },
        contentType: MIME_TYPES.JSON,
        webId: 'system'
      }, { parentCtx: ctx });
    }
  },
  methods: {
    async onFollow(ctx, activity, trainingUri) {
      const registrations = await ctx.call('minicourses.registrations.getRunning', {
        trainingUri,
        actorUri: activity.actor
      });
      if (registrations.length === 0) {
        await ctx.call('minicourses.registrations.post', {
          resource: {
            type: 'tutor:Registration',
            'tutor:registrationFor': trainingUri,
            'tutor:registrant': activity.actor,
            'heco:startDate': (new Date()).toISOString(),
            'pair:hasStatus': urlJoin(this.settings.baseUrl, 'status', 'running')
          },
          contentType: MIME_TYPES.JSON,
          webId: 'system'
        });
      }
    },
    async onUnfollow(ctx, activity, trainingUri) {
      const registrations = await ctx.call('minicourses.registrations.getRunning', { trainingUri, actorUri: activity.actor });
      if( registrations.length > 0 ) {
        for( let registration of registrations ) {
          await ctx.call('minicourses.registrations.put', {
            resourceUri: registration.id,
            resource: {
              ...registration,
              'pair:hasStatus': urlJoin(this.settings.baseUrl, 'status', 'aborted')
            },
            contentType: MIME_TYPES.JSON,
            webId: 'system'
          });
        }
      }
    }
  },
  events: {
    async 'activitypub.inbox.received'(ctx) {
      const { activity, recipients } = ctx.params;
      const trainingsContainerUri = await this.actions.getContainerUri({}, { parentCtx: ctx });
      for( let actorUri of recipients ) {
        if( getContainerFromUri(actorUri) === trainingsContainerUri ) {
          if( activity.type === ACTIVITY_TYPES.FOLLOW ) {
            await this.onFollow(ctx, activity, actorUri);
          } else if ( activity.type === ACTIVITY_TYPES.UNDO && activity.object.type === ACTIVITY_TYPES.FOLLOW ) {
            await this.onUnfollow(ctx, activity, actorUri);
          }
        }
      }
    }
  },
  hooks: {
    before: {
      create(ctx) {
        ctx.params.resource['pair:hasStatus'] = urlJoin(this.settings.baseUrl, 'status', 'unavailable');
      }
    },
    after: {
      put(ctx, res) {
        if(
          res.oldData['pair:hasStatus'] === urlJoin(this.settings.baseUrl, 'status', 'unavailable') &&
          res.newData['pair:hasStatus'] === urlJoin(this.settings.baseUrl, 'status', 'available')
        ) {
          this.actions.announce({ trainingUri: res.resourceUri }, { parentCtx: ctx });
        }
        return res;
      }
    }
  }
};
