const QueueService = require('moleculer-bull');
const CoursesService = require("./services/courses");
const TrainingsService = require("./services/trainings");
const CompetencesService = require("./services/competences");
const JobsService = require("./services/jobs");
const LessonsService = require("./services/lessons");
const MailerService = require("./services/mailer");
const RegistrationsService = require("./services/registrations");

module.exports = {
  name: 'minicourses',
  settings: {
    baseUrl: null,
    frontendUrl: null,
    containersPath: '',
    queueServiceUrl: null,
    // Services customization
    courses: {},
    trainings: {},
    jobs: {},
    competences: {},
    lessons: {},
    mailer: {
      from: null,
      transport: {
        host: null,
        port: null,
        secure: null,
        auth: {
          user: null,
          pass: null,
        },
      }
    },
    registrations: {}
  },
  created() {
    const { baseUrl, frontendUrl, containersPath, queueServiceUrl } = this.settings;

    this.subservices = {};

    this.subservices.courses = this.broker.createService(CoursesService, {
      settings: {
        baseUrl,
        path: containersPath + '/courses',
        ...this.settings.courses
      },
    });

    this.subservices.trainings = this.broker.createService(TrainingsService, {
      settings: {
        baseUrl,
        path: containersPath + '/trainings',
        ...this.settings.trainings
      },
    });

    this.subservices.jobs = this.broker.createService(JobsService, {
      settings: {
        baseUrl,
        path: containersPath + '/jobs',
        ...this.settings.jobs
      },
    });

    this.subservices.competences = this.broker.createService(CompetencesService, {
      settings: {
        baseUrl,
        path: containersPath + '/competences',
        ...this.settings.competences
      },
    });

    this.subservices.lessons = this.broker.createService(LessonsService, {
      settings: {
        path: containersPath + '/lessons',
        ...this.settings.lessons,
      },
    });

    this.subservices.mailer = this.broker.createService(MailerService, {
      mixins: queueServiceUrl ? [QueueService(queueServiceUrl)] : undefined,
      settings: {
        baseUrl,
        frontendUrl,
        ...this.settings.mailer
      },
    });

    this.subservices.registrations = this.broker.createService(RegistrationsService, {
      settings: {
        baseUrl,
        path: containersPath + '/registrations',
        ...this.settings.registrations
      },
    });
  }
};