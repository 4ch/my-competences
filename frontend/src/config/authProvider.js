import { authProvider as semappsAuthProvider } from '@semapps/auth-provider';
import dataProvider from "./dataProvider";

const authProvider = semappsAuthProvider({
  dataProvider,
  authType: "sso",
  checkPermissions: true
});

export default authProvider;
