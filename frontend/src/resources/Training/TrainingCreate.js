import React from 'react';
import Create from '../../layout/create/Create';
import TrainingForm from './TrainingForm';
import { useGetIdentity } from 'react-admin';

const TrainingCreate = () => {
  const { data: identity, isLoading: identityLoading } = useGetIdentity();
  if (identityLoading) return <>Loading...</>;

  const transform = (dataTransform) => {
    return ({
    ...dataTransform,
    'heco:isTrainingOf': identity.id
  })}

  return (
  <Create redirect="show" transform={transform}>
    <TrainingForm />
  </Create>
  )
}

export default TrainingCreate;
