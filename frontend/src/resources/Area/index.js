export default {
  dataModel: {
    types: ['heco:Area'],
    list: {
      predicates: ['pair:label']
    }
  }
};
