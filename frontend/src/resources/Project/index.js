export default {
  dataModel: {
    types: ['heco:Project'],
    list: {
      predicates: ['pair:label']
    }
  }
};
