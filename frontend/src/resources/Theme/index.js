export default {
  dataModel: {
    types: ['heco:Topic'],
    list: {
      predicates: ['pair:label']
    }
  }
};
