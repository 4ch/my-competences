export default {
  dataModel: {
    types: ['heco:StudiedObject'],
    list: {
      predicates: ['pair:label']
    }
  }
};
