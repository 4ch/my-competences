export default {
  dataModel: {
    types: ['pair:JobType'],
    list: {
      predicates: ['pair:label']
    }
  }
};
