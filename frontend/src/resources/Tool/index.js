export default {
  dataModel: {
    types: ['heco:Tool'],
    list: {
      predicates: ['pair:label']
    }
  }
};
