export default {
  dataModel: {
    types: ['heco:Discipline'],
    list: {
      predicates: ['pair:label']
    }
  }
};
