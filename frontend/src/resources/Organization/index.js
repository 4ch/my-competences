export default {
  dataModel: {
    types: ['heco:Organization'],
    list: {
      predicates: ['pair:label']
    }
  }
};
