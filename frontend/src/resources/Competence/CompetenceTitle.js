import React from 'react';

const CompetenceTitle = ({ record }) => {
  if (!record) return
  return <span>{ record["pair:label"] }</span>;
};

export default CompetenceTitle;
