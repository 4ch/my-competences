export default {
  dataModel: {
    types: ['heco:Occupation'],
    list: {
      predicates: ['pair:label']
    }
  }
};
