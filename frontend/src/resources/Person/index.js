export default {
  dataModel: {
    types: ['heco:Person'],
    fieldsMapping: {
      title: 'pair:label'
    }
  }
};
