import React from 'react';
import { useCheckAuthenticated } from '@semapps/auth-provider';
import CardsList from "../../common/list/CardsList";
import List from "../../layout/List";
import CommitmentCard from "./CommitmentCard";

const CommitmentList = props => {
  useCheckAuthenticated();

  return (
    <List {...props}>
      <CardsList
        CardComponent={CommitmentCard}
        link="show"
      />
    </List>
  );
}

export default CommitmentList;
