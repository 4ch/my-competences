import React from 'react';

const CommitmentTitle = ({ record }) => {
  return <span>{record ? record['pair:label'] : ''}</span>;
};

export default CommitmentTitle;
