import { useEffect } from 'react';
import { useResourceContext } from 'react-admin';

const ScrollToTop = () => {
  const resource = useResourceContext();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [resource]);

  return null;
};

export default ScrollToTop;
