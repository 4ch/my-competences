import * as React from 'react';
import { Menu, Logout } from 'react-admin';

const MyMenu = () => (
    <Menu>
        <Logout />
    </Menu>
);

export default MyMenu;