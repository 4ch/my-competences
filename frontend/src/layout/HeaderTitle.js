import React from 'react';
import { Typography, Grid } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles((theme) => ({
  title: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    paddingTop: 20,
    paddingBottom: 10,
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.8rem'
    }
  }
}));

const HeaderTitle = ({ children, actions, context, record }) => {
  const classes = useStyles();
  return (
    <Grid container spacing={5}>
      <Grid item xs={6}>
        <Typography variant="h4" color="primary" className={classes.title} id="react-admin-title">
          {typeof children === 'string' ? children : React.cloneElement(children, { record })}
        </Typography>
      </Grid>
      <Grid item xs={6}>
          {actions && React.cloneElement(actions, context)}
      </Grid>
    </Grid>
  );
};

export default HeaderTitle;
