import React from 'react';
import {
  ChipField,
  useResourceContext,
  useListContext,
  sanitizeListRestProps,
  useCreatePath,
  RecordContextProvider,
  Link
} from 'react-admin';
import { LinearProgress } from '@mui/material';
import LaunchIcon from '@mui/icons-material/Launch';
import { useGetExternalLink } from '@semapps/semantic-data-provider';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  link: {
    textDecoration: 'none',
    maxWidth: '100%'
  },
  chipField: {
    maxWidth: '100%'
  },
  addIcon: {
    cursor: 'pointer',
    fontSize: 35,
    position: 'relative',
    top: -2,
    left: -2
  },
  launchIcon: {
    width: 16,
    paddingRight: 6,
    marginLeft: -10
  }
}));

const stopPropagation = e => e.stopPropagation();

// Our handleClick does nothing as we wrap the children inside a Link but it is
// required by ChipField, which uses a Chip from material-ui.
// The material-ui Chip requires an onClick handler to behave like a clickable element.
const handleClick = () => {};

const ChipList = props => {
  const {
    classes: classesOverride,
    className,
    children,
    linkType = 'edit',
    component = 'div',
    primaryText,
    appendLink,
    externalLinks = false,
    ...rest
  } = props;
  const { data, isLoading } = useListContext(props);
  const resource = useResourceContext(props);
  const getExternalLink = useGetExternalLink(externalLinks);
  const createPath = useCreatePath();

  const classes = useStyles(props);
  const Component = component;

  if (isLoading) return <LinearProgress />;

  return (
    <Component className={classes.root} {...sanitizeListRestProps(rest)}>
      {data.map(record => {
        if (!record) return null;
        const externalLink = getExternalLink(record.id);
        if (externalLink) {
          return (
            <RecordContextProvider value={record} key={record.id}>
              <a
                href={externalLink}
                target="_blank"
                rel="noopener noreferrer"
                className={classes.link}
                onClick={stopPropagation}
              >
                <ChipField
                  record={record}
                  resource={resource}
                  source={primaryText}
                  className={classes.chipField}
                  color="primary"
                  deleteIcon={<LaunchIcon className={classes.launchIcon} />}
                  // Workaround to force ChipField to be clickable
                  onClick={handleClick}
                  // Required to display the delete icon
                  onDelete={handleClick}
                />
              </a>
            </RecordContextProvider>
          );
        } else if (linkType) {
          return (
            <RecordContextProvider value={record} key={record.id}>
              <Link className={classes.link} to={createPath( {resource:resource, id:record.id, type:linkType })} onClick={stopPropagation}>
                <ChipField
                  record={record}
                  resource={resource}
                  source={primaryText}
                  className={classes.chipField}
                  color="primary"
                  // Workaround to force ChipField to be clickable
                  onClick={handleClick}
                />
              </Link>
            </RecordContextProvider>
          );
        } else {
          return (
            <RecordContextProvider value={record} key={record.id}>
              <ChipField
                record={record}
                resource={resource}
                source={primaryText}
                className={classes.chipField}
                color="primary"
                // Workaround to force ChipField to be clickable
                onClick={handleClick}
              />
            </RecordContextProvider>
          );
        }
      })}
    </Component>
  );
};

export default ChipList;
