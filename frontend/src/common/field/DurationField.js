import React from 'react';

const DurationField = ({ record, resource, basePath, source, ...rest }) => {
  return <span {...rest}>{record?.[source] || 0} jours</span>
};

export default DurationField;
