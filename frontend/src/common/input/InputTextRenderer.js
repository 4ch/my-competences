import React from 'react';

// Abandonned due to warning of MUI : "The `getOptionLabel` method of Autocomplete returned object in spite of string"

const InputTextRenderer = ( { choice, optionText, ...rest } ) => {
    console.log("InputTextRenderer =", { choice, optionText, rest });
    let choiceToDisplay = "";

    if (choice.type === "heco:Competence")
    {
        choiceToDisplay = choice?.id.substring(choice?.id.lastIndexOf('/') + 1)
    }
    else
    {
        choiceToDisplay = choice[optionText]
    }

    return choiceToDisplay;
};

export default InputTextRenderer;